# App Lifecycle

## Core/Development Incubation

1.  Submit to Incubation
      - Submit to Incubation under [Incubation/Submissions](Incubation/Submissions/README.md)
      - Release and Design Team check based on the ["Acceptance into Incubation"](ReviewProcedures.md) procedure.
2.  Join incubation stage
      - If this app is supposed to potentially replace another Core/Development app, inform the maintainers of the existing app about the now incubating app.
      - Move to `Incubator/` namespace.
      - Usually change app ID to `org.gnome.<codename>`. *The app ID can be retained, even if the project is rejected or at some point remove from GNOME Core.*
      - Public announcement of incubation
3.  Incubation stage
      - Work towards a shippable application that fulfills the [Core App Criteria](AppCriteria.md).
      - Collect feedback from stakeholders like distributions.
      - Potentially publish on Flathub under the code name for feedback.
4.  Submit to GNOME Core
      - Submit under [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues) for review.
      - Release and Design Team will review the app based on the [Core App Criteria](AppCriteria.md).
5.  Core acceptance
      - Move to `GNOME/<codename>` namespace.
      - On rejection see "Core App Rejection" below.

## Core App Removal/Replacement/Rejection

  - In the event that an application is removed from Core, the maintainers should create at least one release with a non-generic app name, reverting to the codename or something else.
  - If the app is rejected in the Incubation phase it will be removed from `/Incubation`. The maintainers can decide to continue the project under the codename or any other name elsewhere.
  - In both cases the app ID of the form `org.gnome.<codename>` can be retained by the project after the removal from Core.

### Conflict with Maintainers

If the app Maintainers and Release Team or Design Team arrive irreconcilable differences about the app and the maintainers want to continue app outside of Core:

  - The app can be replaced with a new app.
  - The app can be forked and used with a new code name in Core. Form of attribution should be clarified with original maintainers.
  - Since the app is owned by its maintainers, it cannot be overtaken as is.
