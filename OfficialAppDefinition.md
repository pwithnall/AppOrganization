# Official GNOME App Definition

GNOME Core apps and GNOME Development tools are “Official GNOME Software” as defined by the [GNOME Foundation’s Software Policy](https://wiki.gnome.org/Foundation/SoftwarePolicy). This document defines the general idea behind those definitions. The detailed requirements are laid out in the [App Criteria](AppCriteria.md). Technical instructions for maintainers can be found in the [Module Requirements ](https://wiki.gnome.org/ReleasePlanning/ModuleRequirements).

## GNOME Core Apps

GNOME Core apps are part of the product delivered by the GNOME Project. They are characterized by the following features:

  - The GNOME Project recommends that all Core apps are installed by default as part of the desktop.
  - The functionality of Core apps is fundamental enough to justify an installation by default.
  - The development of Core apps happens in collaboration between maintainers, contributors, and the Design, and Release Team.
  - Contributions from the wider community are considered for inclusion by the project maintainers.
  - Core apps fulfill high standards regarding quality and continuity.
  - Often, Core apps exhibit or require a tighter integration with the system than other apps.

In general, Core apps are still owned by their maintainers. They usually also possess, apart from their generic name, a codename that continuously identifies the project.

## GNOME Development Tools

GNOME Development tools are tailored to build and design apps for the GNOME ecosystem. They are characterized by the following features:

  - Development tools often integrate design policies or concepts specific to the GNOME ecosystem, enabling the implementation of those policies and concepts.
  - Development tools are often developed in conjunction with the GNOME Project’s onboarding strategy.
  - The GNOME Project does not recommend installing Development tools by default.
