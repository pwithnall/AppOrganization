# App Organization

The documents in this repository describe how apps in GNOME are organized. The documents describe the practical implementation of the [GNOME Foundation's Software Policy](https://wiki.gnome.org/Foundation/SoftwarePolicy). The respective parts of this repository are maintained by the [Release Team](https://gitlab.gnome.org/Teams/Releng) and the [Circle Committee](https://gitlab.gnome.org/Teams/Circle/).

### [App Criteria](AppCriteria.md)

Criteria that are used to decide if apps are suited for GNOME Core, GNOME Development, or GNOME Circle.

### [App Lifecycle](AppLifecycle.md)

Currently, mostly explains the steps for the Incubation process for Core apps and Development tools.

### [Official GNOME App Definition](OfficialAppDefinition.md)

Defines the essence of being a GNOME Core app and a GNOME Development tool.

### [Review Procedures](ReviewProcedures.md)

Defines the steps that reviewers should follow.

## Other Resources

- [Submission to GNOME Circle](https://gitlab.gnome.org/Teams/Circle)
- [Submission to Incubator](https://gitlab.gnome.org/Incubator/Submission) (GNOME Core apps and GNOME Development tools)
- [GNOME Build Metadata](https://gitlab.gnome.org/GNOME/gnome-build-meta)